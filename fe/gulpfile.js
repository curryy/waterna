var less = require('gulp-less');
var concat = require('gulp-concat');
var gulp = require('gulp');
var gulpSequence = require('gulp-sequence');
var jshint = require('gulp-jshint');
var gulpWatch = require('gulp-watch');
var uglify = require('gulp-uglify');
var connect = require('gulp-connect');
var bower = require('gulp-bower');
var versionAppend = require('gulp-version-append');
var rename = require("gulp-rename");


var settings = {
  less_compress: false,
  js_compress: false
};

var distribute = './dist';



var src_less = [
  './app/directives/*/*.less',
	'./app/modules/*/*.less',
	'./app/modules/*/*/*.less'
];

var src_vendor = [
  './bower_components/jquery/dist/jquery.min.js',
  './bower_components/angular/angular.min.js',
  './bower_components/angular-sanitize/angular-sanitize.min.js',
  './bower_components/angular-ui-router/release/angular-ui-router.min.js',
  './bower_components/angular-aria/angular-aria.js',
  './bower_components/angular-animate/angular-animate.js',
  './bower_components/bootstrap/dist/js/bootstrap.min.js',
  './bower_components/angular-bootstrap/ui-bootstrap.min.js',
  './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
  './bower_components/angular-material/angular-material.js',
  './bower_components/angular-touch/angular-touch.min.js',
  './bower_components/angular-messages/angular-messages.min.js',
    './node_modules/ng-facebook/ngFacebook.js',
    './node_modules/chart.js/dist/Chart.js'
];


src_appjs = [
    './app/app.js',
    './app/config.js',
    './app/settings.js',
    './app/routes.js',
    './app/services/*.js',

    './app/directives/*.js',
    './app/directives/*/*.js',
    './app/directives/*/*/*.js',


    './app/modules/*/*.js',
    './app/modules/*/*/*.js',
    './app/modules/*/*/*/*.js'

];



var pipes = {};

pipes.less = function() {
  return gulp.src(src_less)
    .pipe(less({compress: settings.less_compress}))
    .pipe(gulp.dest(distribute));
};

pipes.appjs = function() {
    var nice = gulp.src(src_appjs)
      .pipe(concat('app.js'));
    if(settings.js_compress){
      return nice.pipe(uglify())
        .pipe(gulp.dest(distribute));
    }else{
      return nice.pipe(gulp.dest(distribute));
    }
};

pipes.vendorjs = function() {
  var nice = gulp.src(src_vendor)
    .pipe(concat('vendor.js'));
  if(settings.js_compress){
    return nice.pipe(uglify())
      .pipe(gulp.dest(distribute));
  }else{
    return nice.pipe(gulp.dest(distribute));
  }
};


pipes.jshint = function() {
  return gulp.src(src_appjs)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
};

gulp.task('watch-less',function() {
  gulpWatch(src_less)
    .pipe(less({compress: settings.less_compress}))
    .pipe(gulp.dest(distribute));
});

gulp.task('watch-jshint',function() {
  gulpWatch(src_appjs)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('watch-appjs',function() {
  gulp.watch(src_appjs, pipes.appjs);
});

gulp.task('watch-vendorjs', function() {
  gulp.watch(src_vendor, pipes.vendorjs );
});


gulp.task('connect', function() {
  connect.server({
    root: './',
    hostname: '0.0.0.0',
    livereload: false,
    fallback: './index.html'
  });
});

gulp.task('bower', function() {
  return bower();
});




gulp.task('less', pipes.less);
gulp.task('appjs', pipes.appjs);
gulp.task('vendorjs', pipes.vendorjs);
gulp.task('jshint', pipes.jshint);
gulp.task('build', gulpSequence('appjs', 'vendorjs', 'less'));
gulp.task('publish', gulpSequence('bower', 'build'));
gulp.task('default', ['build', 'watch-less', 'watch-appjs', 'watch-jshint', 'connect']);
