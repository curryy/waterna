(function() {
    'use strict';

    angular
    .module('app')
    .constant('settings', {
        'backend': 'http://localhost:3000/'
    });
})();
