(function() {
    'use strcit';

    angular
    .module('app')
    .factory('authService', authService);

    authService.$inject = [
        '$log', 
        '$http', 
        '$q', 
        '$rootScope'];

    function authService($log, $http, $q, $rootScope) {
        var _logged = false;
        var _user;
        var factory = {
            authenticate: authenticate
        };
        return factory;

        function authenticate(requireLogin) {
            var deferred = $q.defer();
            console.log($rootScope.user);
            if($rootScope.user !== undefined){
                if(requireLogin){
            	   deferred.resolve(true);
                }else{
                    deferred.reject(false);
                }
                return deferred.promise;
            }
            deferred.reject(false);
            return deferred.promise;
        }



    }
})();