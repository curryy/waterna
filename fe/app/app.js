angular.module('app', [
    'ui.router',
    'ngSanitize',
    'ngMaterial',
    'ngFacebook'
])

.run([
    '$rootScope',
    '$state',
    'authService',

    function($rootScope, $state, authService) {

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


    }]);
