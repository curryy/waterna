(function(){
    'use strict';

    angular
    .module('app')
    .directive('adfooter', adfooter);

    function adfooter() {
        return {
            restrict: 'E',
            templateUrl: 'app/directives/footer/footer.html',
            controller: function($scope){

            }
        };        
    }

})();