(function(){
    'use strict';

    angular
    .module('app')
    .directive('adheader', adheader);

    function adheader() {
        return {
            restrict: 'E',
            templateUrl: 'app/directives/header/header.html',
            controller: function($scope, $rootScope){

                $scope.sign = "Zaloguj się";

                $scope.getUser = function() {

                }

                $rootScope.$on('$stateChangeSuccess',
                    function() {
                        $scope.getUser();
                })

            }
        };        
    }

})();