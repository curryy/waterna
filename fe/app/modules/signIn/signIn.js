(function() {
    'use strict';

    angular
    .module('app')
    .controller('signInCtrl', signInCtrl);

    signInCtrl.$inject =  [
        '$http',
        '$state',
        'settings',
        '$rootScope',
        '$mdDialog',
        '$facebook'
    ];


    function signInCtrl($http, $state, settings, $rootScope, $mdDialog, $facebook) {
        var vm = this;

        vm.signin = signin;

        vm.clickFacebook = function() {
          var lol = 'fblogin'
            $facebook.getLoginStatus().then(function (response) {
                if (response.status != 'connected')
                    $facebook.login();
                    var data = { fblogin: response.authResponse.accessToken};
                    console.log(data);
                    $http({
                      url:settings.backend + 'fblogin',
                      method:'POST',
                      data: data,
                    }
                    ).then(function(response) {
                      $rootScope.userId = response.data._id;
                      $state.go('start');
                    });
            });
        };
        function signin() {
            var url = settings.backend + 'signIn',
                data = {
                    email: vm.login,
                    password: vm.password
                };
                console.log(data);
            $http.post(url, data)
            .then(function(response){
                console.log(response);
            }, function(response){
                console.log(response);
            });
        }

    }
})();
