(function() {
    'use strict';

    angular
    .module('app')
    .controller('startCtrl', startCtrl);

    startCtrl.$inject =  [
        '$http',
        '$state',
        'settings',
        '$rootScope',
    ];

    function startCtrl($http, $state, settings, $rootScope) {
        var vm = this;

        vm.response = {};

        var url = settings.backend + '/staff/';
        // vm.getResponse = function() {
        //     $http.get(url)
        //     .then(function(response){
        //         vm.response = response;
        //     }, function(response){
        //         vm.response = response;
        //     })
        // }
        // vm.getResponse();

        vm.addWater = function (amount) {
            console.log("Added water : " + amount);
            var url = settings.backend + 'water',
                data = {
                water: amount,
                userId: $rootScope.userId
                };
            console.log(data);
            $http({
              method:'POST',
              url: url,
              data: data
            }).then(function(response){
                    console.log(response);
                }, function(response){
                    console.log(response);
                });
        }

    };
})();
