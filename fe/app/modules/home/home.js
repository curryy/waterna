(function() {
    'use strict';

    angular
    .module('app')
    .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject =  [
        '$http',
        '$state',
        'settings',
        '$document'
    ];

    var quotes = [{quote : "Drinking water is essential to a healthy lifestyle.", author : "Stephen Curry"}];
    function homeCtrl($http, $state, settings, $document) {
        var vm = this;
        vm.quotes = quotes;
        var ctx = $document[0].getElementById('myChart').getContext('2d');

        var labels = [], month1 = [], month2 = [];
        for(var i= 1; i <32; i++){
            labels.push(i);
            month1.push(Math.floor((Math.random() * 7) + 1))
            month2.push(Math.floor((Math.random() * 7) + 1))
        }
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'This month',
                    data: month1,
                    backgroundColor: "rgba(153,255,51,0.4)"
                }, {
                    label: 'Last month',
                    data: month2,
                    backgroundColor: "rgba(255,153,0,0.4)"
                }]
            }
        });

    }
})();

