(function () {
    'use strict';
    angular.module('app')
        .config(config);
    config.$inject = ['$facebookProvider'];

    function config ($facebookProvider){
        $facebookProvider.setAppId(430089760673221);
        $facebookProvider.setPermissions('email');
    }

})();