(function() {
    'use strict';

    angular
    .module('app')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider, $urlMatcherFactoryProvider, $locationProvider) {

        $urlRouterProvider.otherwise('/');
        $urlMatcherFactoryProvider.strictMode(false);

        $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'app/modules/home/home.html',
            controller: 'homeCtrl as homeCtrl'
        });
        $stateProvider
            .state('start', {
            url: '/start',
            templateUrl: 'app/modules/start/start.html',
            controller: 'startCtrl as startCtrl'
        });


        $stateProvider
        .state('sign', {
            url: '/sign',
            templateUrl: 'app/modules/signIn/signIn.html',
            controller: 'signInCtrl as signInCtrl'
        });


        $locationProvider.html5Mode(true);


    });
})();