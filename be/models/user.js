var mongoose = require('mongoose'),
mongooseApiQuery = require('mongoose-api-query'),
createdModified = require('mongoose-createdmodified').createdModifiedPlugin;

var userSchema = new mongoose.Schema({
  id: mongoose.Schema.ObjectId,
  name: String,
  email: String,
  password: String,
  weight: 'number',
  fbtoken: String,
});

userSchema.plugin(mongooseApiQuery);
userSchema.plugin(createdModified, { index: true });

var User = mongoose.model('User', userSchema);
module.exports = User;
