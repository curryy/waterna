var mongoose = require('mongoose'),
mongooseApiQuery = require('mongoose-api-query'),
createdModified = require('mongoose-createdmodified').createdModifiedPlugin;

var waterSchema = new mongoose.Schema({
  id: mongoose.Schema.ObjectId,
  date: Date,
  water: Number,
  userId: Number,
});

waterSchema.plugin(mongooseApiQuery);
waterSchema.plugin(createdModified, { index: true });
var Water = mongoose.model('Water', waterSchema);
module.exports = Water;
