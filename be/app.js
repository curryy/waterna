var config        = require('./config'),
restify       = require('restify'),
bunyan        = require('bunyan'),
winston       = require('winston'),
bunyanWinston = require('bunyan-winston-adapter'),
mongoose      = require('mongoose');

var _      = require('lodash'),
errors = require('restify-errors');

var User = require('./models/user.js');
var Water = require('./models/water.js');
global.log = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: 'info',
      timestamp: function() {
        return new Date().toString();
      },
      json: true
    }),
  ]
});
global.server = restify.createServer({
  name    : config.name,
  version : config.version,
  log     : bunyanWinston.createAdapter(log),
});

server.use(restify.jsonBodyParser({ mapParams: true }));
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser({ mapParams: true }));
server.use(restify.fullResponse());

server.on('uncaughtException', function(req, res, route, err) {
  log.error(err.stack);
  res.send(err);
});

server.listen(config.port, function() {
  mongoose.connection.on('error', function(err) {
    log.error('Mongoose default connection error: ' + err)
    process.exit(1)
  });

  mongoose.connection.on('open', function(err) {

    if (err) {
      log.error('Mongoose default connection error: ' + err)
      process.exit(1)
    }

    log.info(
      '%s v%s ready to accept connections on port %s in %s environment.',
      server.name,
      config.version,
      config.port,
      config.env
    )
    server.post('/water', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      var body = req.body;
      var water = new Water();
      water.date = new Date();
      water.water = (JSON.parse(body)).water;
      water.userId = (JSON.parse(body)).userId;
      water.save(function() {
        res.send(water);
      });
    });
    server.get('/water/:user', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      Water.find({userId: req.params.user}, function(err, data) {
        if (err) {
          log.error(err)
          return next(new errors.InvalidContentError(err.errors.name.message))
        }
        res.send(data);
        next();
      });
    });
    server.post('/user', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      User.find({email: (JSON.parse(req.body)).email}, function(err, data) {
        var body = req.body;
        var user = new User();
        user.password = (JSON.parse(body)).password;
        user.name = (JSON.parse(body)).name;
        user.fbToken = (JSON.parse(body)).fbToken;
        user.email = (JSON.parse(body)).email;
        if (err) {
          log.error(err)
          return next(new errors.InvalidContentError(err.errors.name.message))
        } if(data !== [])
            res.send('USER_EXIST');
            else {
              user.save(function() {
                res.send(user);
              });
            }
    });
    });

    server.post('/signIn', function(req,res,next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      User.find({email: (JSON.parse(req.body)).email,
        password: (JSON.parse(req.body)).password}, function(err, data) {
        if (err) {
          log.error(err)
          return next(new errors.InvalidContentError(err.errors.name.message))
        }
        res.send(data);
        next();
      });
    });
    server.post('/fblogin', function(req,res,next) {
      console.log(req.body);
      var lol = req.body;
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      User.find({fbtoken: lol.fbtoken }, function(err, data) {
        if (err) {
          log.error(err)
          return next(new errors.InvalidContentError(err.errors.name.message))
        }
        if(data == []) {
          var user = new User();
          user.fbToken = lol.fbToken;
          user.email = lol.email;
          if (err) {
            log.error(err)
            return next(new errors.InvalidContentError(err.errors.name.message))
          }
          user.save(function() {
            res.send(user);
          });
        }
        else {
          res.send(data[0]);
        }
        res.send(data);
        next();
      });
    });
  });
});
global.db = mongoose.connect(config.db.uri);
